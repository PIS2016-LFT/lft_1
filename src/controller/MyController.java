package controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import main.domain.Tarea;
import main.domain.Usuario;

@Controller
public class MyController {

	@RequestMapping("/bienvenido")
	public ModelAndView helloSpring(HttpServletRequest post) {
		String message = "";
		String nombre = post.getParameter("nombre");
		String pass = post.getParameter("pass");

		Usuario u = new Usuario(nombre, pass, "standard");

		String button = post.getParameter("button");

		if ("Registrar".equals(button)){
			if (u.read()){
				message = "No se puede registrar porque ya existe tu nombre ";
				return new ModelAndView("bienvenido", "message", message);
			}
			else {
				message = "Se ha registrado correctamente, ";
				u.create();
				return new ModelAndView("bienvenido", "message", message);
			}

		} else if ("Acceder".equals(button)){
			if (u.read()) 
			{
				if (u.getDAO().getCollection().get(0).equals(u)) 
				{ 
					if (! u.getDAO().getCollection().get(0).getRol().equals("admin")) {
						message =  "Nombre y pass coinciden, pasa primo! ";

						Tarea t = new Tarea();
						t.readByUser(u.getNombre());

						String tareas = "";
						for (Tarea t_tmp: t.getDAO().getCollection()) {
							tareas += "<option>" + t_tmp.toString() + "</option>";
						}
						String[] vector = {tareas, u.getNombre()};
						return new ModelAndView("vista_tareas", "vector", vector);

					} else {
						//Vista de admin
						Usuario user = new Usuario();
						user.readAll();

						String usuarios = "";
						for (Usuario u_tmp: user.getDAO().getCollection()) {
							usuarios += "<option>"+u_tmp.toString() + "</option>";
						}

						return new ModelAndView("vista_admin", "usuarios", usuarios);
					}
				}
				else
				{
					message = "Usurio existe pero contraseņa no coincide ";
					return new ModelAndView("bienvenido", "message", message);
				}
			}
			else 
			{
				message = "Usuario o contraseņa incorrectas ";
				return new ModelAndView("bienvenido", "message", message);
			}	
		}

		return new ModelAndView("vista_tarea", "message", message);
	}

	@RequestMapping("/tarea_creada")
	public ModelAndView tarea(HttpServletRequest post) {
		String button = post.getParameter("button");
		String message="Error";
		if ("Crear tarea".equals(button)){
			String nombre = post.getParameter("nombre");
			String usuario = post.getParameter("usuario");
			String prioridad = post.getParameter("prioridad");
			String fecha = post.getParameter("fecha");
			String proyecto = post.getParameter("proyecto");
			String notas = post.getParameter("notas");
			String estado = post.getParameter("estado");
			
			Tarea t = new Tarea(nombre, usuario, prioridad, fecha, proyecto, notas, estado);
			t.create();
			message = "fallo al crear la tarea";
			if (t.read()) message = "tarea creada correctamente";

			return new ModelAndView("tarea_creada", "message", message);
		}
		if("Borrar tarea".equals(button)){
			String n = post.getParameter("sometext");
			
			Tarea t=getParametersTarea(n);								
			t.delete();
			if(!t.read()) 
				message="la tarea se ha borrado";

			return new ModelAndView("tarea_creada", "message", message);
		}
		if("Modificar tarea".equals(button)){
			String n = post.getParameter("sometext");
			Tarea tareaVieja=getParametersTarea(n);
		
			String nombre = post.getParameter("nombre");
			String usuario = post.getParameter("usuario");
			String prioridad = post.getParameter("prioridad");
			String fecha = post.getParameter("fecha");
			String proyecto = post.getParameter("proyecto");
			String notas = post.getParameter("notas");
			String estado = post.getParameter("estado");
			
			Tarea tareaNueva = new Tarea(nombre, usuario, prioridad, fecha, proyecto, notas, estado);
			tareaVieja.update(tareaNueva);
			
			if(tareaNueva.read()) 
				message="la tarea se ha modificado";
			
			return new ModelAndView("tarea_creada", "message", message);
						
		}
		
		return new ModelAndView("tarea_creada", "message", message);
	}
	
	@RequestMapping("/usuario_creado")
	public ModelAndView UsuarioCreado(HttpServletRequest post) {
		String button = post.getParameter("button");
		String message="Error";
		if ("Crear usuario".equals(button)){
			String nombre = post.getParameter("nombre");
			String pass = post.getParameter("pass");
			String rol = post.getParameter("rol");
			
			Usuario u=new Usuario(nombre, pass, rol);
			u.create();
			
			if (u.read()) message = "Usuario creado correctamente ";

			return new ModelAndView("tarea_creada", "message", message);
		}
		if("Borrar usuario".equals(button)){
			String n = post.getParameter("listaUsuarios");
			
			Usuario u=getParametersUsuario(n);								
			u.delete();
			if(!u.read()) 
				message="El usuario se ha borrado";

			return new ModelAndView("tarea_creada", "message", message);
		}
		if("Modificar usuario".equals(button)){
			String n = post.getParameter("listaUsuarios");
			Usuario usuarioViejo=getParametersUsuario(n);	
		
			String nombre = post.getParameter("nombre");
			String pwd = post.getParameter("pass");
			String rol = post.getParameter("rol");
			
			Usuario usuarioNuevo= new Usuario(nombre, pwd, rol);
			
			usuarioViejo.delete();
			usuarioNuevo.create();
			
			if(usuarioNuevo.read()) 
				message="El usuario se ha modificado";
			
			return new ModelAndView("tarea_creada", "message", message);
						
		}
		return new ModelAndView("tarea_creada", "message", message);
	}
	
	public Tarea getParametersTarea(String n){
		String[] n1 = n.split(": ");
		String[] n2 = new String[8];
		int i = 0;
		for (String s: n1){
			n2[i] = s.split(",")[0];
			i++;
		}
		String[] n3 = Arrays.copyOfRange(n2, 1, n2.length);
		String nombre, usuario, prioridad, fecha, proyecto, notas, estado;
		nombre = n3[0];
		usuario = n3[1];
		prioridad = n3[2];
		fecha = n3[3];
		proyecto = n3[4];
		notas = n3[5];
		estado = n3[6];
	
		Tarea t = new Tarea(nombre, usuario, prioridad, fecha, proyecto, notas, estado);
		
		return t;
	}
	public Usuario getParametersUsuario(String n){
		String[] n1 = n.split(": ");
		String[] n2 = new String[4];
		int i = 0;
		for (String s: n1){
			n2[i] = s.split(",")[0];
			i++;
		}
		
		String[] n3 = Arrays.copyOfRange(n2, 1, n2.length);
		String nombre, pwd, rol;
		nombre = n3[0];
		pwd = n3[1];
		rol = n3[2];
	
	
		Usuario u = new Usuario(nombre, pwd, rol);
		
		return u;
	}

	
}