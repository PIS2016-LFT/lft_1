package main.domain;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

import main.persistance.MongoDBBroker;

public class DAOTarea {

	private ArrayList<Tarea> _collection;
	private String _tabla;

	//https://www.tutorialspoint.com/mongodb/mongodb_java.htm	

	public DAOTarea() {
		this._collection = new ArrayList<Tarea>();
		this._tabla = "Tareas";
	}

	public boolean read(Tarea tarea) { //refactorizar a un find to guapo
		this._collection = new ArrayList<Tarea>();
		
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);
		BasicDBObject doc = new BasicDBObject();
		doc.append("nombre", tarea.getNombre());
		doc.append("usuario",tarea.getUsuario());
		doc.append ("prioridad", tarea.getPrioridad());
		doc.append ("fecha", tarea.getFecha_fin());
		doc.append ("proyecto", tarea.getProyecto());
		doc.append ("notas", tarea.getNotas());
		doc.append ("estado", tarea.getEstado());
		
		DBCursor cursor = coll.find(doc);
		DBObject tmpobj;
		String nombre, usuario, prioridad, fecha_fin, proyecto, notas, estado;
		Tarea t;
		
		if (!cursor.hasNext()) return false;
				
		while(cursor.hasNext()) {
			tmpobj = cursor.next();
			nombre = tmpobj.get("nombre").toString(); 
			usuario = tmpobj.get("usuario").toString();
			prioridad = tmpobj.get("prioridad").toString();
			fecha_fin = tmpobj.get("fecha").toString();
			proyecto = tmpobj.get("proyecto").toString();
			notas = tmpobj.get("notas").toString();
			estado = tmpobj.get("estado").toString();
			t = new Tarea(nombre, usuario, prioridad, fecha_fin, proyecto, notas, estado);
			this._collection.add(t);
		}
		return true;
	}
		
	

	public void readAll(Tarea tarea) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);
		DBCursor cursor = coll.find();

		Tarea t;
		String nombre, usuario, prioridad, fecha_fin, proyecto, notas, estado;
		DBObject tmpobj;
		while(cursor.hasNext()) {
			tmpobj = cursor.next();
			nombre = tmpobj.get("nombre").toString(); 
			usuario = tmpobj.get("usuario").toString();
			prioridad = tmpobj.get("prioridad").toString();
			fecha_fin = tmpobj.get("fecha").toString();
			proyecto = tmpobj.get("proyecto").toString();
			notas = tmpobj.get("notas").toString();
			estado = tmpobj.get("estado").toString();
			t = new Tarea(nombre, usuario, prioridad, fecha_fin, proyecto, notas, estado);
			this._collection.add(t);
		}
	}


	public void create(Tarea task) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);

		BasicDBObject doc = new BasicDBObject();
		doc.append ("nombre", task.getNombre());
		doc.append ("usuario", task.getUsuario());
		doc.append ("prioridad", task.getPrioridad());
		doc.append ("fecha", task.getFecha_fin());
		doc.append ("proyecto", task.getProyecto());
		doc.append ("notas", task.getNotas());
		doc.append ("estado", task.getEstado());

		coll.insert(doc);
	}

	public int delete(Tarea task) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);

		BasicDBObject doc = new BasicDBObject("nombre", task.getNombre());
		doc.append("usuario", task.getUsuario());
		doc.append("prioridad", task.getPrioridad());
		doc.append("fecha", task.getFecha_fin());
		doc.append("proyecto", task.getProyecto());
		doc.append("notas", task.getNotas());
		doc.append("estado", task.getEstado());

		WriteResult result = coll.remove(doc);
		return result.getN();
	}
	
	public void update(Tarea taskVieja, Tarea taskNueva){
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);

		BasicDBObject searchquery = new BasicDBObject();
		searchquery.append("nombre", taskVieja.getNombre());
		searchquery.append("usuario", taskVieja.getUsuario());
		searchquery.append("prioridad", taskVieja.getPrioridad());
		searchquery.append("fecha", taskVieja.getFecha_fin());
		searchquery.append("proyecto", taskVieja.getProyecto());
		searchquery.append("notas", taskVieja.getNotas());
		searchquery.append("estado", taskVieja.getEstado());
		
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.append("nombre", taskNueva.getNombre());
		newDocument.append("usuario", taskNueva.getUsuario());
		newDocument.append("prioridad", taskNueva.getPrioridad());
		newDocument.append("fecha", taskNueva.getFecha_fin());
		newDocument.append("proyecto", taskNueva.getProyecto());
		newDocument.append("notas", taskNueva.getNotas());
		newDocument.append("estado", taskNueva.getEstado());

		coll.update(searchquery, newDocument);

	}

	public boolean readByUser(String username) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);
		BasicDBObject doc = new BasicDBObject("usuario", username);
		DBCursor cursor = coll.find(doc);

		if (!cursor.hasNext()) return false;

		Tarea t;
		String nombre, usuario, prioridad, fecha, proyecto, notas, estado;
		DBObject tmpobj;

		while(cursor.hasNext()) {
			tmpobj = cursor.next();
			nombre = tmpobj.get("nombre").toString(); 
			usuario = tmpobj.get("usuario").toString();
			prioridad = tmpobj.get("prioridad").toString();
			fecha = tmpobj.get("fecha").toString();
			proyecto = tmpobj.get("proyecto").toString();
			notas = tmpobj.get("notas").toString();
			estado = tmpobj.get("estado").toString();
			
			t = new Tarea(nombre, usuario,prioridad, fecha ,proyecto, notas, estado);
			this._collection.add(t);
		}
		
		return true;
		
	} 

	public ArrayList<Tarea> getCollection(){
		return this._collection;
	}


}