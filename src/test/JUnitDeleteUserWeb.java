package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitDeleteUserWeb {

	WebDriver driver = new ChromeDriver();
	Usuario user = new Usuario("manoli", "1234", "standard");
	Usuario admin = new Usuario("admin", "admin", "admin");
	
	@Given("^The admin has at least one user$")
	public void The_admin_has_at_least_one_user() throws Throwable {
		user.create();
		admin.create();
		
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
		driver.findElement(By.name("nombre")).sendKeys(admin.getNombre());
		driver.findElement(By.name("pass")).sendKeys(admin.getPwd());
		driver.findElement(By.xpath("//input[@value='Acceder']")).click();
	}

	@When("^The admin selects it and clicks on Delete button$")
	public void The_admin_selects_it_and_clicks_on_Delete_button() throws Throwable {
		Select dropdown = new Select(driver.findElement(By.name("listaUsuarios"))); //sometext == selector
		dropdown.selectByVisibleText(user.toString());
		
		driver.findElement(By.xpath("//input[@value='Borrar usuario']")).click();
	}

	@Then("^The user will be deletedD$")
	public void The_user_will_be_deletedD() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("El usuario se ha borrado"));
	    driver.quit();
	}
}
