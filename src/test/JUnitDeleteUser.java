package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitDeleteUser {
	
	Usuario user;

	@Given("^The admin has a user$")
	public void The_admin_has_a_user() throws Throwable {
		user = new Usuario ("Manoli", "1234", "standard");
		user.create();
		assertTrue(user.read());
	}

	@When("^The admin deletes a user$")
	public void The_admin_deletes_a_user() throws Throwable {
		user.delete();
	}

	@Then("^The user is not in the database$")
	public void The_user_is_not_in_the_database() throws Throwable {
		assertFalse(user.read());
		
		tearDown();
	}
	
	public void tearDown() {
		user.delete();
	}
}
