package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Tarea;

public class JUnitDeleteTask {

	Tarea tasktest;

	@Given("^The user has a  task$")
	public void The_user_has_a_task() throws Throwable {
		tasktest = new Tarea ("Mandar email", "manolo", "alta", "12/12/2013", "proyecto1", "notas", "sin empezar");
		tasktest.create();
		assertTrue(tasktest.read());
	}

	@When("^The user deletes a task$")
	public void The_user_deletes_a_task() throws Throwable {
		tasktest.delete();
	}

	@Then("^The task is not in the database$")
	public void The_task_is_not_in_the_database() throws Throwable {
		assertFalse(tasktest.read());
		
		tearDown();
	}
	
	public void tearDown() {
		tasktest.delete();
	}
	
}