package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnnitDeleteTaskWeb {
	
	WebDriver driver = new ChromeDriver();
	Tarea t = new Tarea("tareaN", "pedroma", "prioridad_alta", "ma�ana", "proyecto1", "notas1", "sin terminar");
	Usuario u = new Usuario("pedroma", "1234", "standard");
	
	@Given("^The user has at least one task$")
	public void The_user_has_at_least_one_task() throws Throwable {
		t.create();
		
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
		driver.findElement(By.name("nombre")).sendKeys(u.getNombre());
		driver.findElement(By.name("pass")).sendKeys(u.getPwd());
		driver.findElement(By.xpath("//input[@value='Acceder']")).click();
	}

	@When("^The user selects it and clicks on Delete button$")
	public void The_user_selects_it_and_clicks_on_Delete_button() throws Throwable {
		Select dropdown = new Select(driver.findElement(By.name("sometext"))); //sometext == selector
		dropdown.selectByVisibleText(t.toString());
		
		driver.findElement(By.xpath("//input[@value='Borrar tarea']")).click();
	}

	@Then("^The task will be deleted$")
	public void The_task_will_be_deleted() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("la tarea se ha borrado"));
	    driver.quit();
	}
	
}
