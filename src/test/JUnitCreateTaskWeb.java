package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Tarea;
import main.domain.Usuario;


public class JUnitCreateTaskWeb {

	WebDriver driver = new ChromeDriver();
	Tarea t = new Tarea("tareaN", "pedroma", "prioridad_alta", "ma�ana", "proyecto1", "notas1", "sin terminar");
	Usuario u = new Usuario("pedroma", "1234", "standard");
	
	@Given("^The user has entered in its account$")
	public void The_user_has_entered_in_its_account() throws Throwable {
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
		driver.findElement(By.name("nombre")).sendKeys(u.getNombre());
		driver.findElement(By.name("pass")).sendKeys(u.getPwd());
		driver.findElement(By.xpath("//input[@value='Acceder']")).click();
	}

	@When("^The user fills the gaps with the appropriate information$")
	public void The_user_fills_the_gaps_with_the_appropriate_information() throws Throwable {
		driver.findElement(By.name("nombre")).sendKeys(t.getNombre());;
		driver.findElement(By.name("usuario")).sendKeys(t.getUsuario());
		driver.findElement(By.name("prioridad")).sendKeys(t.getPrioridad());
		driver.findElement(By.name("fecha")).sendKeys(t.getFecha_fin());
		driver.findElement(By.name("proyecto")).sendKeys(t.getProyecto());
		driver.findElement(By.name("notas")).sendKeys(t.getNotas());
		driver.findElement(By.name("estado")).sendKeys(t.getEstado());

		driver.findElement(By.xpath("//input[@value='Crear tarea']")).click();

	}

	@Then("^The task will be stored$")
	public void The_task_will_be_stored() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("tarea creada correctamente"));
	    driver.quit();

		tearDown();
	}
	
	public void tearDown(){
		t.delete();
	}

}
