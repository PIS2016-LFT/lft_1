package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

import static org.junit.Assert.assertTrue;

public class registeredWeb {

	WebDriver driver = new ChromeDriver();
	Usuario laura = new Usuario("laura","1234","standard");
	Usuario pedroma = new Usuario("pedroma","1234","standard");

	@Given("^The user opens the webpage$")
	public void The_user_goes_to_the_webpage() throws Throwable {
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
	}

	@When("^The user inserts an unregistered identification$")
	public void The_user_inserts_an_unregistered_identification() throws Throwable {
		
		WebElement input_nombre = driver.findElement(By.name("nombre"));
		input_nombre.sendKeys(laura.getNombre());
		
		WebElement input_pwd = driver.findElement(By.name("pass"));
		input_pwd.sendKeys(laura.getPwd());
		
		WebElement boton = driver.findElement(By.xpath("//input[@value='Registrar']"));
		boton.click();
	}

	@Then("^The user will be informed about their registration$")
	public void The_user_will_be_informed_about_their_registration() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("Se ha registrado correctamente"));
	}

	@When("^The user inserts a already registered identification$")
	public void The_user_inserts_a_already_registered_identification() throws Throwable {
		WebElement input_nombre = driver.findElement(By.name("nombre"));
		input_nombre.sendKeys(pedroma.getNombre());
		
		WebElement input_pwd = driver.findElement(By.name("pass"));
		input_pwd.sendKeys(pedroma.getPwd());
		
		WebElement btn_registrar = driver.findElement(By.xpath("//input[@value='Registrar']"));
		btn_registrar.click();
	}

	@Then("^The user will be warned with this fact$")
	public void The_user_will_be_warned_with_this_fact() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("No se puede registrar porque ya existe tu nombre"));
	}
	
	@Then("^The browser closes$")
	public void The_browser_gets_closed() throws Throwable {
	    driver.quit();

	    tearDown();
	}
	
	public void tearDown() {
		laura.delete();
	}
	
}
