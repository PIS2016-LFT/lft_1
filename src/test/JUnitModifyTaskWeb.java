package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Tarea;
import main.domain.Usuario;

public class JUnitModifyTaskWeb {
  
  WebDriver driver = new ChromeDriver();
  Usuario u = new Usuario("pedroma", "1234", "standard");
  Tarea t = new Tarea("t", "pedroma", "1", "12/12/12", "1", "notas", "sin terminar");
  Tarea t2 = new Tarea("t2", "pedroma", "2", "2/2/2", "2", "notas2", "sin terminar");
  
  @Given("^The user opens the webpage and is loged$")
  public void The_user_opens_the_webpage_and_is_loged() throws Throwable {
	  t.create();
	  System.setProperty("webdriver.chrome.driver","chromedriver.exe");
    
    driver.get("localhost:8080/Sprint_1");
    driver.findElement(By.name("nombre")).sendKeys(u.getNombre());
    driver.findElement(By.name("pass")).sendKeys(u.getPwd());
    driver.findElement(By.xpath("//input[@value='Acceder']")).click();
    
  
  }

  @When("^The user modifies some data of this task in the web$")
  public void The_user_modifies_some_data_of_this_task_in_the_web() throws Throwable {
	  
    driver.findElement(By.name("nombre")).sendKeys(t2.getNombre());
	driver.findElement(By.name("usuario")).sendKeys(t2.getUsuario());
	driver.findElement(By.name("prioridad")).sendKeys(t2.getPrioridad());
	driver.findElement(By.name("fecha")).sendKeys(t2.getFecha_fin());
	driver.findElement(By.name("proyecto")).sendKeys(t2.getProyecto());
	driver.findElement(By.name("notas")).sendKeys(t2.getNotas());
	driver.findElement(By.name("estado")).sendKeys(t2.getEstado());

	Select dropdown = new Select(driver.findElement(By.name("sometext")));
	dropdown.selectByVisibleText(t.toString());
    
    driver.findElement(By.xpath("//input[@value='Modificar tarea']")).click();
  
  }

  @Then("^The task is correctly modified$")
  public void The_task_is_correctly_modified() throws Throwable {
    WebElement cabecera = driver.findElement(By.id("cabecera"));
    assertTrue(cabecera.getText().contains("la tarea se ha modificado"));
  }

  @Then("^The browser closesM$")
  public void The_browser_closesM() throws Throwable {
     driver.quit();
     
     tearDown();
  }
  
  public void tearDown() {
    t.delete();
    t2.delete();
  }
  
}