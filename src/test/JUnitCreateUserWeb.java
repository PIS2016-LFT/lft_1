package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitCreateUserWeb {
	
	WebDriver driver = new ChromeDriver();
	Usuario user = new Usuario("manoli", "1234", "standard");
	Usuario admin = new Usuario("admin", "admin", "admin");
	

	@Given("^The admin has entered in its account$")
	public void The_admin_has_entered_in_its_account() throws Throwable {
		admin.create();
		
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
		driver.findElement(By.name("nombre")).sendKeys(admin.getNombre());
		driver.findElement(By.name("pass")).sendKeys(admin.getPwd());
		driver.findElement(By.xpath("//input[@value='Acceder']")).click();
	}

	@When("^The admin insert all data in the web$")
	public void The_admin_insert_all_data_in_the_web() throws Throwable {
		driver.findElement(By.name("nombre")).sendKeys(user.getNombre());;
		driver.findElement(By.name("pass")).sendKeys(user.getPwd());
		driver.findElement(By.name("rol")).sendKeys(user.getRol());
	
		driver.findElement(By.xpath("//input[@value='Crear usuario']")).click();
	}

	@Then("^The user will be stored$")
	public void The_user_will_be_stored() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("Usuario creado correctamente"));
	    driver.quit();

		tearDown();
	}
	
	public void tearDown(){
		user.delete();
	}
}
