package test;

import static org.junit.Assert.assertFalse;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitLogin2 {

	Usuario user_right, user_wrong;

	@Given("^the user has registered in the system$")
	public void the_user_has_registered_in_the_system() throws Throwable {
		user_right = new Usuario ("Lopez", "1234", "standard");
		user_right.create();
	}

	@When("^the user inserts a wrong identification$")
	public void the_user_inserts_a_wrong_identification() throws Throwable {
		user_wrong = new Usuario ("manolo", "12345", "standard");

	}

	@Then("^The user will not be granted access to the system$")
	public void The_user_will_not_be_granted_access_to_the_system() throws Throwable {
		assertFalse(user_wrong.read());

		tearDown();
	}

	public void tearDown() {
		user_right.delete();
	}
}