package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitModifyUserWeb {
	
	WebDriver driver = new ChromeDriver();
	Usuario user = new Usuario("manoli", "1234", "standard");
	Usuario user2 = new Usuario("pepa", "1234", "standard");
	Usuario admin = new Usuario("admin", "admin", "admin");
	
	@Given("^The admin opens the webpage and is loged$")
	public void The_admin_opens_the_webpage_and_is_loged() throws Throwable {
		admin.create();
		user.create();
		
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
		driver.findElement(By.name("nombre")).sendKeys(admin.getNombre());
		driver.findElement(By.name("pass")).sendKeys(admin.getPwd());
		driver.findElement(By.xpath("//input[@value='Acceder']")).click();
	
	}

	@When("^The admin modifies some data of this user in the web$")
	public void The_admin_modifies_some_data_of_this_user_in_the_web() throws Throwable {
		driver.findElement(By.name("nombre")).sendKeys(user2.getNombre());;
		driver.findElement(By.name("pass")).sendKeys(user2.getPwd());
		driver.findElement(By.name("rol")).sendKeys(user2.getRol());
	
		Select dropdown = new Select(driver.findElement(By.name("listaUsuarios")));
		dropdown.selectByVisibleText(user.toString());
		
		driver.findElement(By.xpath("//input[@value='Modificar usuario']")).click();
	}

	@Then("^The user is correctly modified$")
	public void The_user_is_correctly_modified() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
	    assertTrue(cabecera.getText().contains("El usuario se ha modificado"));
	  
	}

	@Then("^The browser closesMm$")
	public void The_browser_closesMm() throws Throwable {
		driver.quit();
	     
	     tearDown();
	}
	
	public void tearDown() {
	    user.delete();
	    user2.delete();
	  }
}
