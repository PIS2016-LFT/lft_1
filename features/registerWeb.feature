Feature: Register into the system from the web

	Scenario: Valid user
		Given The user opens the webpage
		When The user inserts an unregistered identification
		Then The user will be informed about their registration
		And The browser closes
		
	Scenario: Invalid user
		Given The user opens the webpage
		When The user inserts a already registered identification
		Then The user will be warned with this fact
		And The browser closes

		